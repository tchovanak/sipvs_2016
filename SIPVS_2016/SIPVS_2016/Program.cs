﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Schema;

namespace SIPVS_2016
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new InvoiceForm());

            //Validator.validateXML(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
            //                                "\\schemes\\XMLSchema.xsd",
            //                                Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
            //                                ".\\schemes\\XML_SAMPLE.xml");
            Transformator.transformXMLtoHTML(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                            "\\schemes\\XSLTFile.xslt",
                                            Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                            ".\\schemes\\XML_SAMPLE.xml",
                                            Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                            ".\\schemes\\HTML_NEW.html");
        }



        
    }
}
