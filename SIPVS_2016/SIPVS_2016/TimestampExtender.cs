﻿using System;
using System.Text;
using System.Xml;
using System.IO;
using Org.BouncyCastle.Asn1.Tsp;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Tsp;
using Org.BouncyCastle.Cms;

namespace SIPVS_2016
{
    public class TimestampExtender
    {
        // transform xml through xslt to html 
        public static void extendToTForm(string idFaktura, string pathToEPES, string pathToOutput)
        {

            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            doc.Load(pathToEPES);
            doc.Save(pathToOutput + "xades-t-faktura_1" + idFaktura + ".xml");

            XmlNodeList sigValueList = doc.GetElementsByTagName("ds:SignatureValue");
            var value = sigValueList.Item(0);
            var ts = new TS.TSSoapClient();
            //var tsRespAsn = ts.GetTimestamp(Convert.ToBase64String(Encoding.UTF8.GetBytes(value.InnerText)));
            var tsRespAsn = ts.GetTimestamp(value.InnerText);
            var tsResp = new TimeStampResponse(Convert.FromBase64String(tsRespAsn));
            var tst = tsResp.TimeStampToken.GetEncoded();
            // for debugging
            File.WriteAllText(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                            "\\outputs\\asn1_faktura.xml", tsRespAsn);
            var tst2 = new TimeStampToken(new CmsSignedData(tst));
            // tst vlozime do elementu xzep:DataEnvelope/ds:Signature/ds:Object/xades:QualifyingProperties/
            // vytvorime novy element xades:UnsignedProperties s id = ds:Signature/@id + UnsignedProperties
            // donho vlozime novy element UnsignedSignatureProperties
            // donho vlozime novy element SignatureTimeStamp s atributom @id
            // donho vlozime novy element xades:EncapsulatedTimeStamp
            // donho vlozime vygenerovany token ako Base64 string
            // to je cele
            var nodeQualProps = doc.GetElementsByTagName("xades:QualifyingProperties").Item(0);
            var unsPropsList = doc.GetElementsByTagName("xades:UnsignedProperties");
            var unsSigPropsList = doc.GetElementsByTagName("xades:UnsignedSignatureProperties");
            var id = doc.GetElementsByTagName("ds:Signature").Item(0).Attributes.Item(0).InnerText;

            XmlNode sigTS = doc.CreateElement("xades", "SignatureTimeStamp", "http://uri.etsi.org/01903/v1.3.2#");
            var attIdTS = doc.CreateAttribute("id");
            attIdTS.Value = id + "SignatureTimeStamp";
            sigTS.Attributes.Append(attIdTS);
            XmlNode encTS = doc.CreateElement("xades", "EncapsulatedTimeStamp", "http://uri.etsi.org/01903/v1.3.2#");
            encTS.InnerText = Convert.ToBase64String(tst);
            sigTS.AppendChild(encTS);

            XmlNode unsProps = null;
            if (unsPropsList.Count == 0) // unsigned properties already exists
            {
                unsProps = doc.CreateElement("xades","UnsignedProperties", "http://uri.etsi.org/01903/v1.3.2#");
                var attId = doc.CreateAttribute("id");
                attId.Value = id + "UnsignedProperties";
                unsProps.Attributes.Append(attId);
                nodeQualProps.AppendChild(unsProps);
            } else
            {
                unsProps = unsPropsList.Item(0);
            }
            XmlNode unsSigProps = null;
            if (unsSigPropsList.Count == 0) // unsigned properties already exists
            {
                unsSigProps = doc.CreateElement("xades", "UnsignedSignatureProperties", "http://uri.etsi.org/01903/v1.3.2#");
                unsSigProps.AppendChild(sigTS);
                unsProps.AppendChild(unsSigProps);
            }
            else
            {
                unsSigProps = unsSigPropsList.Item(0);
                unsSigProps.AppendChild(sigTS);
            }

            doc.Save(pathToOutput + "xades-t-faktura_" + idFaktura + ".xml");
            System.Windows.Forms.MessageBox.Show("Podpis s casovou peciatkou bol ulozeny do " + pathToOutput + "xades-t-faktura_" + idFaktura + ".xml");
            //XmlNode unsigProps = xmlDoc.CreateElement("issue_date");

        }

        private static TimeStampResp readTimeStampResp(Asn1InputStream input)
        {
            try
            {
                return TimeStampResp.GetInstance(input.ReadObject());
            }
            catch (ArgumentException e)
            {
                throw new TspException("malformed timestamp response: " + e, e);
            }
            catch (InvalidCastException e)
            {
                throw new TspException("malformed timestamp response: " + e, e);
            }
        }
    }

    
}
