﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SIPVS_2016
{
    class Util
    {
        public static Dictionary<string, string> parseIssuerName(string issuerName)
        {
            var dict = new Dictionary<string, string>();
            string[] splitted = issuerName.Split(new char[]{ ',', '='});
            for (int i = 0; i < splitted.Length - 1; i += 2)
            {
                string val = "";
                if (dict.ContainsKey(splitted[i]))
                {
                    dict.TryGetValue(splitted[i], out val);
                }
                dict[splitted[i].Trim()] =  val + splitted[i + 1].Trim();
            }
            return dict;
        }
    }
}
