﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.IO;
using System.Windows.Forms;

namespace SIPVS_2016
{
    class Transformator
    {
        // transform xml through xslt to html 
        public static void transformXMLtoHTML(string xslPath, string xmlPath, string htmlPath)
        {
            // open .xml file as an XPathDocument and create write for .html file
            if (File.Exists(xmlPath))
            {
                XPathDocument doc = new XPathDocument(xmlPath);

                XmlWriter writer = XmlWriter.Create(htmlPath);
           
                // create and load the transform
                XslCompiledTransform transform = new XslCompiledTransform();
                XsltSettings settings = new XsltSettings();
                settings.EnableScript = true;
                transform.Load(xslPath, settings, null);

                // execute the transformation
                transform.Transform(doc, writer);
                writer.Close();
            }else
            {
                return;
            }
            
        }
    }
}
