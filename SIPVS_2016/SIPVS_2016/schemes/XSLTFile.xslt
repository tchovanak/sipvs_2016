<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:dn="http://www.fiit.stuba.sk/SIPVS"
    xmlns:exsl="http://exslt.org/common"
    extension-element-prefixes="exsl"
    exclude-result-prefixes="exsl"
>
  <xsl:output method="html" indent="yes"/>

  <xsl:template match="/">
    <html>
      <head>
        <style>
          .invoice-box{
          max-width:800px;
          margin:auto;
          padding:30px;
          border:1px solid #eee;
          box-shadow:0 0 10px rgba(0, 0, 0, .15);
          font-size:16px;
          line-height:24px;
          font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
          color:#555;
          }

          .invoice-box table{
          width:100%;
          line-height:inherit;
          text-align:left;
          }

          .invoice-box table td{
          padding:5px;
          vertical-align:top;
          }

          .invoice-box table tr td:nth-child(2){
          text-align:right;
          }

          .invoice-box table tr.top table td{
          padding-bottom:20px;
          }

          .invoice-box table tr.top table td.title{
          font-size:45px;
          line-height:45px;
          color:#333;
          }

          .invoice-box table tr.information table td{
          padding-bottom:40px;
          }

          .invoice-box table tr.heading td{
          background:#eee;
          border-bottom:1px solid #ddd;
          font-weight:bold;
          }

          .invoice-box table tr.details td{
          padding-bottom:20px;
          }

          .invoice-box table tr.item td{
          border-bottom:1px solid #eee;
          }

          .invoice-box table tr.item.last td{
          border-bottom:none;
          }

          .invoice-box table tr.total td:nth-child(2){
          border-top:2px solid #eee;
          font-weight:bold;
          }

          @media only screen and (max-width: 600px) {
          .invoice-box table tr.top table td{
          width:100%;
          display:block;
          text-align:center;
          }

          .invoice-box table tr.information table td{
          width:100%;
          display:block;
          text-align:center;
          }
          }
        </style>
      </head>
      <body>
        <div class="invoice-box">
          <table cellpadding="0" cellspacing="0">
            <tr class="top">
              <td colspan="6">
                <table>
                  <tr>
                    <td class="title">
                      Faktúra - daňový doklad
                    </td>

                    <td>
                      číslo faktúry : <xsl:value-of select="dn:invoice/@id"/><br/>
                      Dátum vystavenia: <xsl:value-of select="dn:invoice/dn:actual_date"/><br/>
                      Dátum splatnosti: <xsl:value-of select="dn:invoice/dn:issue_date"/>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

            <tr class="information">
              <td colspan="6">
                <table>
                  <tr>
                    <td>
                      <b>Dodávateľ :</b>
                      <br></br>
                      <xsl:value-of select="dn:invoice/dn:provider/@name"/>
                      <br></br>
                      <xsl:value-of select="dn:invoice/dn:provider/dn:street"/>
                      <br></br>
                      <xsl:value-of select="dn:invoice/dn:provider/dn:zip"/> &#160; &#160; <xsl:value-of select="dn:invoice/dn:provider/dn:city"/>
                      <br></br>
                      <xsl:value-of select="dn:invoice/dn:provider/dn:state"/>
                      <br></br>
                      DIC: <xsl:value-of select="dn:invoice/dn:provider/dn:DIC"/>
                      <br></br>
                      IC:  <xsl:value-of select="dn:invoice/dn:provider/dn:IC"/>
                      <br></br>
                      email: <xsl:value-of select="dn:invoice/dn:provider/dn:email"/>
                      <br></br>
                      Tel: <xsl:value-of select="dn:invoice/dn:provider/dn:phone"/>
                    </td>

                    <td>
                      <b>Odberateľ:</b>
                      <br></br>
                      <xsl:value-of select="dn:invoice/dn:consumer/@name"/>
                      <br></br>
                      <xsl:value-of select="dn:invoice/dn:consumer/dn:street"/>
                      <br></br>
                      <xsl:value-of select="dn:invoice/dn:consumer/dn:zip"/> &#160; &#160; <xsl:value-of select="dn:invoice/dn:consumer/dn:city"/>
                      <br></br>
                      <xsl:value-of select="dn:invoice/dn:consumer/dn:state"/>
                      <br></br>
                      DIC: <xsl:value-of select="dn:invoice/dn:consumer/dn:DIC"/>
                      <br></br>
                      IC:  <xsl:value-of select="dn:invoice/dn:consumer/dn:IC"/>
                      <br></br>
                      email: <xsl:value-of select="dn:invoice/dn:consumer/dn:email"/>
                      <br></br>
                      Tel: <xsl:value-of select="dn:invoice/dn:consumer/dn:tel"/>
                    </td>

                  </tr>
                </table>
              </td>
            </tr>

            <tr class="heading">
              <td>
                Položka
              </td>
              <td>
                počet ks
              </td>
              <td>
                jednotková cena (EUR)
              </td>
              <td>
                cena bez DPH (EUR)
              </td>
              <td>
                sadzba DPH (EUR)
              </td>
              <td>
                cena s DPH (EUR)
              </td>
            </tr>
            <xsl:for-each select="dn:invoice/dn:items/dn:item">
              <tr class="item">
                <td>
                  <xsl:value-of select="."/>
                </td>
                <td>
                  <xsl:value-of select="@units"/>
                </td>
                <td>
                  <xsl:value-of select="@unit_price"/>
                </td>
                <td>
                  <xsl:value-of select="(@unit_price * @units)" />
                </td>
                <td>
                  <xsl:value-of select="@tax" />
                </td>
                <td>
                  <!--<xsl:variable name="pricewithtax" select="(@unit_price * @units)*((100 + @tax) div 100)" />
                  <xsl:value-of select="$pricewithtax" />-->
                  <xsl:value-of select="@unit_price_with_tax"/>
                </td>
              </tr>
            </xsl:for-each>
            <xsl:variable name="items">
              <xsl:for-each select="dn:invoice/dn:items/dn:item">

                <number>
                  <xsl:value-of select="(@unit_price * @units)*((100 - @tax) div 100)" ></xsl:value-of>
                </number>
              </xsl:for-each>
            </xsl:variable>


            <tr class="total">
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>
                Total:
                <xsl:call-template name="plus">
                  <xsl:with-param name="node" select="dn:invoice/dn:items/dn:item[1]"/>
                </xsl:call-template>
              </td>
            </tr>

            <tr>
              <td colspan="6">
                <b>Zodpovedná osoba:</b>
                <xsl:value-of select="dn:invoice/dn:responsible_person"></xsl:value-of>
              </td>
            </tr>
          </table>
        </div>
      </body>

    </html>
  </xsl:template>

  <!-- from http://www.kosek.cz/xml/xslt/exslt.html#d5e2866-->
  <xsl:template name="plus">
    <xsl:param name="node" select="."/>
    <xsl:variable name="sumother">
      <xsl:choose>
        <xsl:when test="$node/following-sibling::item">
          <xsl:call-template name="plus">
            <xsl:with-param name="node"
              select="$node/following-sibling::item[1]"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          0
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="($node/@unit_price * $node/@units)*((100 - $node/@tax) div 100) + $sumother"/>
  </xsl:template>

</xsl:stylesheet>
