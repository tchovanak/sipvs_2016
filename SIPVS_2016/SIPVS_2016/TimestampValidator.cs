﻿using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Cms;
using Org.BouncyCastle.Tsp;
using Org.BouncyCastle.X509;
using System;
using System.Collections;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;

namespace SIPVS_2016
{
    /// <summary>
    /// Trieda na overenie platnosti certifikatu v case. Poskytuje metody na overenie casovej peciatky a platnosti certifikatov podla datumu aj CRL.
    /// </summary>
    public class TimestampValidator
    {


        #region Public methods

        /// <summary>
        /// Overuje ci ma casova peciatka podpisaneho dokumentu platny certifikat a ci nedoslo k zmenam v dokumente po pridani casovej peciatky.
        /// </summary>
        /// <remarks>
        /// Aby bola casova peciatka platna, musi platit, ze v case jej vygenerovania platil certifikat casovej peciatky.
        /// Certifikat casovej peciatky musel byt teda vydany pred vznikom peciatky a expirovat po vzniku casovej peciatky.
        /// Zaroven nemohol byt zruseny (a tym zverejeneny na CRL vydavatela.)
        /// Okrem toho je potrebne skontrolovat, ci nedoslo k zmene dokumentu, cim sa potvrdi, ze <paramref name="dec"/> existoval v case, ktory je v casovej peciatke.
        /// Integritu dokumentu a casovej peciatky overime tak, ze zobereme hodnotu podpisu dokumentu a vyratame z neho hash rovnakym algoritmom akym bol vyratany a ulozeny  case vzniku casovej peciatky.
        /// Ak sa hash, ktory teraz vyratame zhoduje s hashom z peciatky, vieme, ze je vsetko valdine a dokuemnt v takejto podobe existoval v case vzniku peciatky.
        /// </remarks>
        /// <param name="doc">Podpisany XML dokument s casovou peciatkou.</param>
        /// <param name="ns">Namespace dokumentu v ktorom sa maju hladat XML elementy.</param>
        /// <param name="errMsg">Premenna, do ktorej sa naplnia chyby ak nejake nastanu.</param>
        /// <returns>True ak je casova peciatka platna, False ak je neplatna.</returns>
        public static bool ValidateTimestamp(XmlDocument doc, XmlNamespaceManager ns, out string errMsg)
        {

            errMsg = "\n\n TIMESTAMP VALIDATION:";
            try
            {
                bool ret = true;
                // Z casovej peciatky najdem podpisový certifikát - viem ho ziskat podla timestampTokenu ulozenom v unsigned casti dokuemntu. 
                var encapsulatedTimestamp = doc.GetElementsByTagName("xades:EncapsulatedTimeStamp").Item(0).InnerText; 
                var timestampToken = GetTimeStampToken(encapsulatedTimestamp);
                //tst.ToCmsSignedData().GetSignerInfos().GetSigners()

                var timestampCertificate = GetTimeStampSignatureCertificate(timestampToken);
                var signers = timestampToken.ToCmsSignedData().GetSignerInfos().GetSigners();
                //foreach (var signer in signers)
                //{
                //    var signercasted = (Org.BouncyCastle.Cms.SignerInformation)signer;
                //    if (!signercasted.Verify(timestampCertificate))
                //    {
                //        errMsg += Environment.NewLine + "CHYBA: Certifikát časovej pečiatky:";
                //        ret = false;
                //    }
                //}
                // overi ci je certifikat momentalne platny
                try { 
                    timestampCertificate.CheckValidity();
                }catch(Exception e)
                {
                    errMsg += Environment.NewLine + "CHYBA: Certifikát časovej pečiatky je v tomto case uz neplatny:" + e.Message;
                    ret = false;
                }

                // da sa asi aj toto >
                //Validate the time stamp token.
                //To be valid the token must be signed by the passed in certificate and the certificate must be the one referred to by the SigningCertificate attribute included in the hashed attributes of the token. The certificate must also have the ExtendedKeyUsageExtension with only KeyPurposeId.id_kp_timeStamping and have been valid at the time the timestamp was created.

                //A successful call to validate means all the above are true.
                timestampToken.Validate(timestampCertificate);

                var certificateExpiresAt = timestampCertificate.CertificateStructure.EndDate.ToDateTime();
                var certificateStartAt = timestampCertificate.CertificateStructure.StartDate.ToDateTime();

                // Overenie, ci bol certifikat casovej peciatky platny v case vzniku casovej peciatky
                if (!timestampCertificate.IsValid(timestampToken.TimeStampInfo.GenTime))
                {
                    errMsg += Environment.NewLine + "CHYBA: Certifikát časovej pečiatky bol v čase vygenerovania casovej peciatky neplatný.";
                    errMsg += Environment.NewLine + "Certifikat bol platny od: " + certificateStartAt.ToString() + " do: " + certificateExpiresAt.ToString() + ", Peciatka vygenerovana " + timestampToken.TimeStampInfo.GenTime.ToString();
                    ret = false;
                }

                // Overim ci je certifikat platny v case podpisu
                if (timestampToken.TimeStampInfo.GenTime > certificateExpiresAt || timestampToken.TimeStampInfo.GenTime < certificateStartAt)
                {
                    errMsg += Environment.NewLine + "CHYBA: Certifikát bol v čase podpisu neplatný";
                    ret = false;
                }

                // Overenie ci nebol certifikat casovej peciatky zruesny (podla CRL)
                if (IsCertificateRevoked(timestampCertificate, "http://test.monex.sk/DTCCACrl/DTCCACrl.crl")) // TODO trebalo by ziskavat CRL podla datumu vzniku peciatky a nie brat aktualny...
                {
                    errMsg += Environment.NewLine + "CHYBA: Certifikát časovej pečiatky bol v čase podpisu zruseny";
                    ret = false;

                }

                // Ziskame hodnotu hash z signature value v case podpisu a nazov algoritmu, aby sme mohli vyratat aktualnu poda podpsiu.
                var digestValue = timestampToken.TimeStampInfo.GetMessageImprintDigest();
                var digestMethod = timestampToken.TimeStampInfo.MessageImprintAlgOid;
                var signatureValue = doc.GetElementsByTagName(@"ds:SignatureValue").Item(0).InnerText;
                //var signatureValue = doc.SelectSingleNode(@"//ds:SignatureValue", ns).InnerText;

                var hash = GetHash(digestMethod, Convert.FromBase64String(signatureValue));

                var digestValueString = Convert.ToBase64String(digestValue);
                var hashString = Convert.ToBase64String(hash);

                // Overenie ci bol podpisany taky dokument ako mame teraz.
                if (!digestValueString.Equals(hashString))
                {
                    errMsg += "CHYBA: Zlyhanie overenie msgImprint z casovej peciatky oproti signtaure value. " + digestValueString + " vs." + hashString;
                    ret = false;
                }
                return ret;
            }
            catch (Exception ex)
            {
                errMsg = "\n\n TIMESTAMP VALIDATION: Nastala chyba pri spracovani." + ex.Message;
                return false;
            }

            errMsg = "\n\n TIMESTAMP VALIDATION: OK";
            return true;
        }

        /// <summary>
        /// Overuje, ci bol v case podpisu dokumentu platny certifikat, ktorym bol dokument podpisany. 
        /// </summary>
        /// <param name="doc"> Podpisovany dokument. </param>
        /// <param name="ns"> Namesapace dokumentu, v ktorom sa maju hladat elementy. </param>
        /// <param name="errMsg"> Premenna, do ktorej sa zapisu chyby, ak nejake nastali. </param>
        /// <returns>True ak bo; certifikat platny v  case podpisu, False ak bol neplatny.</returns>
        public static bool ValidateCertificateInCRL(XmlDocument doc, XmlNamespaceManager ns, out string errMsg)
        {
            errMsg = "\n\n CRL VALIDATION:";
            try
            {
                bool ret = true;
                // Najdem v dokumente element certifikatu
                var certificateNode = doc.SelectSingleNode(@"//ds:X509Certificate", ns);
                if (certificateNode == null)
                {
                    errMsg = Environment.NewLine + "CHYBA: Nenašiel sa element ds:X509Certificate";
                    ret = false;
                }
                // Zoberem z neho hodnotu certifikatu
                byte[] certificateValue = Convert.FromBase64String(certificateNode.InnerText);
                var certificateStructure = X509CertificateStructure.GetInstance(Asn1Object.FromByteArray(certificateValue));
                var certificate = new Org.BouncyCastle.X509.X509Certificate(certificateStructure);
                // overi ci je certifikat momentalne platny
                try
                {
                    certificate.CheckValidity();
                }catch(Exception e)
                {
                    errMsg += Environment.NewLine + "CHYBA: Certifikát dokumentu je v tomto case uz neplatny:" + e.Message;
                    ret = false;
                }
                // Z certifikatu vyberiem datum expiracie              
                //var certificateExpiresAt = certificate.CertificateStructure.EndDate.ToDateTime();
                // Ziskam cas podpisu z casovej peciatky
                var encapsulatedTimestamp = doc.GetElementsByTagName("xades:EncapsulatedTimeStamp").Item(0).InnerText; // TODO spravit s namespacmani
                var timestampToken = GetTimeStampToken(encapsulatedTimestamp);
                var documentSignedAt = timestampToken.TimeStampInfo.GenTime;
                var certificateExpiresAt = certificate.CertificateStructure.EndDate.ToDateTime();
                var certificateStartAt = certificate.CertificateStructure.StartDate.ToDateTime();


                //errMsg += Environment.NewLine + "Platny od: " +  certificate.CertificateStructure.StartDate.ToDateTime().ToString() + "do: " + certificateExpiresAt.ToString() + ", podpisane " + documentSignedAt;
                // return false;

                // Overim ci je certifikat platny
                if (!certificate.IsValid(documentSignedAt)) //documentSignedAt >= certificateExpiresAt
                {
                    errMsg += Environment.NewLine + "CHYBA: Certifikát dokumentu je neplatný";
                    ret = false;
                }

                // Overim ci je certifikat platny v case podpisu
                if (documentSignedAt > certificateExpiresAt || documentSignedAt < certificateStartAt)
                {
                    errMsg += Environment.NewLine + "CHYBA: Certifikát bol v čase podpisu neplatný";
                    ret = false;
                }

                // Overenie ci nie je certifikat zruseny 
                if (IsCertificateRevoked(certificate, "http://test.monex.sk/DTCCACrl/DTCCACrl.crl")) // TODO: nemal by som overovat zrusenie v case podpisu? Teda napr. ziskat crl z dna po podpise?
                {
                    errMsg += Environment.NewLine + "CHYBA: Certifikát bol zrušený podľa CRL";
                    ret = false;
                }
                return ret;
            }
            catch(Exception e)
            {
                errMsg += Environment.NewLine + "CHYBA: Pri spracovaní nastala chyba..." + e.Message;
                return false;
            }

            errMsg += " OK";
            return true;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Ziskava certifikat casovej peciatky z timestampTokenu.
        /// </summary>
        /// <remarks>
        /// Ked je vygenerovana casova peciatka, token ktory ju reprezentuje je ulozeny do dokemntu. 
        /// Aby sme vedeli spatne overit platnost casovej peciatky, potrebujeme ziskat certifikat, ktory hovori o pravosti tej casovej peciatky. A na to nam sluzi tato metoda. 
        /// </remarks>
        /// <param name="timestampToken">Token z casovej peciatky ulozeny v dokumente v castu unsigned info. </param>
        /// <returns>Certifikat casovej peciatky.</returns>
        private static Org.BouncyCastle.X509.X509Certificate GetTimeStampSignatureCertificate(TimeStampToken timestampToken)
        {
            try
            {

                var certificatesStore = timestampToken.GetCertificates("Collection");
                var certificatesList = new ArrayList(certificatesStore.GetMatches(null));


                // Najdenie podpisoveho certifikatu 
                foreach (Org.BouncyCastle.X509.X509Certificate cert in certificatesList)
                {
                    string cerIssuerName = cert.IssuerDN.ToString(true, new Hashtable());
                    string signerIssuerName = timestampToken.SignerID.Issuer.ToString(true, new Hashtable());

                    // Kontrola issuer name a serioveho cisla
                    if (cerIssuerName == signerIssuerName && cert.SerialNumber.Equals(timestampToken.SignerID.SerialNumber))
                    {
                        return cert;
                    }
                }
            }
            catch
            {
                return null;
            }

            return null;
        }


        /// <summary>
        /// Vytvara instanciu TimestampToken-u na zaklade jeho hodnoty ulozenej v dokuemnte.
        /// </summary>
        /// <param name="tokenValue">Hodnota tokenu ulozena ako base64 string - tak ako bola ulozena v dokumente.</param>
        /// <returns>Instanciu TimeStamp tokenu</returns>
        private static TimeStampToken GetTimeStampToken(string tokenValue)
        {
            try
            {
                var tokenBytes = Convert.FromBase64String(tokenValue);
                var cmsSignedData = new CmsSignedData(tokenBytes);
                var token = new TimeStampToken(cmsSignedData);

                return token;
            }
            catch
            {
                throw;
            }
        }

        // TODO: do CRL overovania by podla mna mal vstupovat aj datum podpisu, lebo nas zaujima, ci nebol vtedy zruseny a nie ci je teraz zruseny...

        /// <summary>
        /// Overuje, ci nebol certifikat zruseny.
        /// </summary>
        /// <remarks>
        /// Ked je certifikat zruseny skor ako mu konci platnost, musi byt zverejneny na CRL listine tej autority, ktora ho vydala.
        /// Ak teda nenajdeme <paramref name="certicate"/> na crl, ktoru ziskame z <paramref name="crlUrl"/>, mozeme prehlasit ze z pohladu zrusenia je platny.
        /// </remarks>
        /// <param name="certicate">Certifikat, ktoreho platnost chceme overit.</param>
        /// <param name="crlUrl">URL adresa, na ktorej je zverejneny CRL. </param>
        /// <returns>True ak bol certifikat zruseny, False ak nebol zruseny. </returns>
        private static bool IsCertificateRevoked(Org.BouncyCastle.X509.X509Certificate certicate, string crlUrl)
        {

            var crl = new System.Net.WebClient().DownloadData(crlUrl);
            var crlParser = new X509CrlParser();
            var isRevoked = crlParser.ReadCrl(crl).IsRevoked(certicate);

            return isRevoked;
        }

        /// <summary>
        /// Vypocita hodnotu hash z <paramref name="valueToHash"/>zadanym hashovacim algoritmom <paramref name="algoOid"/>
        /// </summary>
        /// <param name="algoOid"> OID definujuce hashovaci algoritmus. </param>
        /// <param name="valueToHash"> Hodnota, ktoru chceme zahashovat. </param>
        /// <returns></returns>
        private static byte[] GetHash(string algoOid, byte[] valueToHash)
        {
            byte[] result = null;
            switch (algoOid)
            {
                case "2.16.840.1.101.3.4.2.1": // "SHA256";
                    var sha256 = SHA256.Create();
                    result = sha256.ComputeHash(valueToHash);
                    break;
                default:
                    throw new NotImplementedException("Pokus o hashobanie neznamym algoritmom");
            }

            return result;
        }

       
        #endregion

    }

}
