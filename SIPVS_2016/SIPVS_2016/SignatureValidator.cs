﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X509;
using System.Windows.Forms;


namespace SIPVS_2016
{
    public static class SignatureValidator
    {

        public static void Validate(string pathToSignature)
        {
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            doc.Load(pathToSignature);
            XmlNamespaceManager ns = new XmlNamespaceManager(doc.NameTable);
            string errorReport = "";
            bool res = true;
            /////////////////////////////// Overenie dátovej obálky//////////////////////////////////////////////////////////
            // koreňový element musí obsahovať atribúty xmlns:xzep a xmlns:ds podľa profilu XADES_ZEP.
            string err = "";
            if (!DataEnvelopeValidation(doc, ns, pathToSignature, out err))
            {
                res = false;
            }
            errorReport += err;
            /////////////////////////////// OVERENIE XML SIGNATURE //////////////////////////////////////////////////////////
            err = "";
            if (!XmlSignatureValidation(doc, ns, pathToSignature, out err))
            {
                res = false;
            }
            errorReport += err;
            /////// CORE VALIDATION 
            // Core validation (podľa špecifikácie XML Signature) – overenie hodnoty podpisu ds:SignatureValue a referencií v ds: SignedInfo:
            err = "";
            if (!CoreValidation(doc, ns, pathToSignature, out err))
            {
                res = false;
            }
            errorReport += err;
            //	overenie ostatných elementov profilu XAdES_ZEP, ktoré prináležia do špecifikácie XML Signature:
            /////// FORMALNE KONTROLY
            err = "";
            if (!XmlSignatureFormalValidation(doc, ns, pathToSignature, out err))
            {
                res = false;
            }
            errorReport += err;
            // overenie referencií v elementoch ds:Manifest:
            err = "";
            if (!ManifestRefsValidation(doc, ns, pathToSignature, out err))
            {
                res = false;
            }
            errorReport += err;
            /////////////////////////////// Overenie časovej pečiatky //////////////////////////////////////////////
            err = "";
            if (!TimestampValidator.ValidateTimestamp(doc, ns, out err))
            {
                res = false;
            }
            errorReport += err;

            /////////////////////////////// Overenie platnosti podpisového certifikátu: ///////////////////////////
            err = "";
            if (! TimestampValidator.ValidateCertificateInCRL(doc, ns, out err))
            {
                res = false;
            }
            errorReport += err;


            ////////////////////////////// VYPIS VYSLEDKOV /////////////////////////////////////////////////
            if (res)
            {
                errorReport = "\n-----------------------------------------------------------------------------------\nSUBOR :" + pathToSignature + " Podpis je platny!";
            }
            else
            {
                errorReport = "\n-----------------------------------------------------------------------------------\nCHYBY V SÚBORE " + pathToSignature + ":" + errorReport;
            }
            System.IO.File.AppendAllText(@"validationOutput.txt", errorReport);
            MessageBox.Show(errorReport);
        }


        private static bool DataEnvelopeValidation(XmlDocument doc, XmlNamespaceManager ns, string pathToSignature, out string err)
        {
            err = "\n\n ENVELOPE VALIDATION: OK";
            // koreňový element musí obsahovať atribúty xmlns:xzep a xmlns:ds podľa profilu XADES_ZEP.
            // Get root element
            XmlElement root = doc.DocumentElement;
            // Verify attributes
            bool valid = verifyRootAttributes(root, out err);
            return valid;
        }

        private static bool verifyRootAttributes(XmlElement root, out string err)
        {
            err = "\n\n ENVELOPE VALIDATION:";
            // koreňový element musí obsahovať atribúty xmlns:xzep a xmlns:ds podľa profilu XADES_ZEP.
            string[] attributes = new string[2] { "xmlns:xzep", "xmlns:ds" };
            for (var i = 0; i < attributes.Length; i++){
                // if element has all the attributes, ok check value
                if (root.HasAttribute(attributes[i])){
                    String attributeValue = root.GetAttribute(attributes[i]);
                    // verify value of attributes (probably needed)
                    if (!verifyAttributeValue(attributeValue)){
                        err += string.Format("\nAttribute value {0} is not valid!", attributeValue); ;
                        return false;
                    }
                }
                else{
                    // negative version, so return false or throw exception
                    err += string.Format("\nAttribute {0} was not found!", attributes[i]); ;
                    return false;
                }
            }
            // everything went well, so return true            
            err += " OK";
            return true;
        }


        private static bool XmlSignatureValidation(XmlDocument doc, XmlNamespaceManager ns, string pathToSignature, out string err)
        {
            err = "\n\n XML SIGNATURE ELEMENTS VALIDATION: OK";
            // kontrola obsahu ds:SignatureMethod a ds:CanonicalizationMethod – musia obsahovať URI niektorého z podporovaných algoritmov pre dané elementy podľa profilu XAdES_ZEP,
            //kontrola obsahu ds:Transforms a ds:DigestMethod vo všetkých referenciách v ds:SignedInfo – musia obsahovať URI niektorého z podporovaných algoritmov podľa profilu XAdES_ZEP,   
            // Verify attributes
            bool valid = verifyXMLSignatureAttributes(doc, out err);
            return valid;
        }


        private static bool verifyXMLSignatureAttributes(XmlDocument doc, out string err)
        {
            bool valid = true;
            err = "\n\nXML SIGNATURE ELEMENTS VALIDATION:";
            // Get canonicalization elements 
            // kontrola obsahu ds:SignatureMethod a ds:CanonicalizationMethod – musia obsahovať URI niektorého z podporovaných algoritmov pre dané elementy podľa profilu XAdES_ZEP,
            XmlNodeList elemList = doc.GetElementsByTagName("ds:CanonicalizationMethod");
            foreach (XmlNode node in elemList){
                if (node.Attributes["Algorithm"] != null){
                    var value = node.Attributes["Algorithm"].Value;
                    if (!verifyCanonicalization(value)){
                        err += string.Format("\nds:CanonicalizationMethod: Incorrect Algorithm attribute value: {0} !", value);
                        valid = false;
                    }
                }
                else{
                    err += string.Format("\nds:CanonicalizationMethod: Algorithm attribute missing!");
                    valid = false;
                }
            }
            // Get signature method elements
            elemList = doc.GetElementsByTagName("ds:SignatureMethod");
            foreach (XmlNode node in elemList){
                if (node.Attributes["Algorithm"] != null){
                    var value = node.Attributes["Algorithm"].Value;
                    if (!verifySignatureMethod(value)){
                        err += string.Format("\nds:SignatureMethod: Incorrect Algorithm attribute value: {0} !", value);
                        valid = false;
                    }
                }
                else{
                    err += string.Format("\nds:SignatureMethod: Algorithm attribute missing!");
                    valid = false;
                }
            }
            //kontrola obsahu ds:Transforms a ds:DigestMethod vo všetkých referenciách v ds:SignedInfo – musia obsahovať URI niektorého z podporovaných algoritmov podľa profilu XAdES_ZEP, 
            // Get ds:SignedInfo elements, than check ds:Transforms and ds:DigestMethod attributes
            string error = "";
            elemList = doc.GetElementsByTagName("ds:SignedInfo");
            foreach (XmlNode node in elemList){
                for (var i = 0; i < node.ChildNodes.Count; i++){
                    var currNode = node.ChildNodes[i];
                    if (currNode.Name == "ds:Reference"){
                        // check attributes
                        if (!checkDsReferenceAttributes(currNode, out error)){
                            // merge error results, if ok then empty
                            err += error;
                            valid = false;
                        }
                    }
                }
            }
            // everything ok, so return first true
            if (valid){
                err += " OK";
            }
            return valid;
        }


        private static bool checkDsReferenceAttributes(XmlNode dsReference, out string error)
        {
            bool valid = true;
            string error1 = "";
            if (!checkDsDigestMethodElement(dsReference, out error1)){
                valid = false;
            }
            string error2 = "";
            if (!checkDsTransformElement(dsReference, out error2)){
                valid = false;
            }
            error = error1 + error2;
            return valid;
        }


        private static bool checkDsTransformElement(XmlNode dsReference, out string error)
        {
            error = "";
            if (dsReference["ds:Transforms"] != null){
                XmlElement elem = dsReference["ds:Transforms"];
                XmlNodeList elemList = elem.GetElementsByTagName("ds:Transform");
                if (elemList.Count != 0){
                    foreach (XmlNode node in elemList){
                        if (node.Name == "ds:Transform"){
                            if (node.Attributes["Algorithm"] != null){
                                var attributeValue = node.Attributes["Algorithm"].Value;
                                if (!verifyCanonicalization(attributeValue)){
                                    error = string.Format("\nds:Transform: Attribute value {0} is not valid!", attributeValue);
                                    return false;
                                }
                            }else{
                                error = string.Format("\nds:Transform element has no Algorithm attribute!");
                                return false;
                            }
                        }
                    }
                }else{
                    error = string.Format("\nds:Transform element not found!");
                    return false;
                }
            }else{
                error = string.Format("\nds:Transforms element not found!");
                return false;
            }
            // ok, so return true, error is empty
            return true;

        }


        private static bool checkDsDigestMethodElement(XmlNode dsReference, out string error)
        {
            error = "";
            if (dsReference["ds:DigestMethod"] != null){
                XmlElement elem = dsReference["ds:DigestMethod"];
                if (elem.HasAttribute("Algorithm")){
                    String attributeValue = elem.GetAttribute("Algorithm");
                    if (!verifyDsDigestMethod(attributeValue)){
                        error = string.Format("\nds:DigestMethod: Attribute value {0} is not valid!", attributeValue);
                        return false;
                    }
                }else{
                    error = string.Format("\nNo Algorithm attribute on ds:DigestMethod!");
                    return false;
                }
            }else{
                error = string.Format("\nds:DigestMethod element not found!");
                return false;
            }
            // ok 
            return true;
        }


        private static bool verifyAttributeValue(String attributeValue)
        {
            string[] attributeValues = new string[2] { "http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0",
                                                       "http://www.w3.org/2000/09/xmldsig#" };
            if (attributeValues.Contains(attributeValue)){return true;}
            return false;
        }


        private static bool verifyCanonicalization(String attributeValue)
        {
            String acceptedValue = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
            if (attributeValue == acceptedValue){return true;}
            return false;
        }


        private static bool verifySignatureMethod(String attributeValue)
        {
            string[] attributeValues = new string[5] { "http://www.w3.org/2000/09/xmldsig#dsa-sha1",
                                                       "http://www.w3.org/2000/09/xmldsig#rsa-sha1",
                                                       "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256",
                                                       "http://www.w3.org/2001/04/xmldsig-more#rsa-sha384",
                                                       "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512" };

            if (attributeValues.Contains(attributeValue)){return true;}
            return false;
        }


        private static bool verifyDsDigestMethod(String attributeValue)
        {
            string[] attributeValues = new string[5] { "http://www.w3.org/2000/09/xmldsig#sha1",
                                                       "http://www.w3.org/2001/04/xmldsig-more#sha224",
                                                       "http://www.w3.org/2001/04/xmlenc#sha256",
                                                       "http://www.w3.org/2001/04/xmldsig-more#sha384",
                                                       "http://www.w3.org/2001/04/xmlenc#sha512" };

            if (attributeValues.Contains(attributeValue)){return true;}
            return false;
        }


        private static bool CoreValidation(XmlDocument doc, XmlNamespaceManager ns, String path, out string errMsg)
        {
            // VALIDATE EVERY REFERENCE IN DS:SIGNED 
            var status = "";
            errMsg = "";
            string err = "";
            // Validate every reference in ds:SignedInfo
            ns.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
            ns.AddNamespace("xzep", "http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0");
            var si = doc.SelectSingleNode("//ds:SignedInfo", ns);
            bool ret1 = ValidateReferencesInNode(si, doc, ns, out err);
            status += err;
            // VALIDATE DS:SIGNATURE 
            err = "";
            bool ret2 = ValidateSignatureValue(doc, ns, path, out err);
            status += err;
            if (status == ""){
                errMsg = "\n\n CORE VALIDATION: OK";
            }else{
                errMsg = "\n\n CORE VALIDATION: " + status;
            }
            if (!ret1 || !ret2){
                return false;
            }else{
                return true;
            }

        }

        private static bool ValidateReferencesInNode(XmlNode node, XmlDocument doc, XmlNamespaceManager ns, out string errMsg)
        {
            errMsg = "";
            var nodes = node.SelectNodes(".//ds:Reference", ns);
            //dereferencovanie URI -prejdem element SIGNED INFO prejdem kazdu referenciu dany objekt si nacitam prevediem transformaciu a porovnam digest value
            foreach (XmlNode nref in nodes){
                var type = nref.Attributes["Type"].Value;
                // dereference uri
                var uri = nref.Attributes["URI"];
                var str = uri.Value.TrimStart(new char[] { '#' });
                XmlDocument innernode = new XmlDocument();
                innernode.PreserveWhitespace = true;
                XmlElement docRoot = doc.DocumentElement;
                XmlNodeList dereferencedNodes = docRoot.SelectNodes(@"//node()[@Id='" + str + "']");
                if (dereferencedNodes.Count == 0){
                    errMsg += "\nCHYBA : Nenasiel sa ziaden referencovany uzol s id : " + str;
                    return false;
                }
                innernode.LoadXml(dereferencedNodes.Item(0).OuterXml);
                // perform all transformations 
                var transformations = nref.SelectNodes(".//ds:Transform", ns);
                //string transformedValue = dereferencedNode.InnerXml;
                Stream s = null;
                foreach (XmlNode transform in transformations){
                    //perform given transformation
                    string alghoritm = transform.Attributes["Algorithm"].Value;
                    switch (alghoritm){
                        case "http://www.w3.org/TR/2001/REC-xml-c14n-20010315":
                            // CANONICAL XML VERSION 1.0
                            XmlDsigC14NTransform t = new XmlDsigC14NTransform();
                            t.Algorithm = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
                            if (s == null){
                                t.LoadInput(innernode);
                            }
                            else{
                                t.LoadInput(s);
                            }
                            //string hashed = Convert.ToBase64String(t.GetDigestedOutput(SHA256.Create()));
                            s = (Stream)t.GetOutput(typeof(Stream));
                            using (Stream output = (Stream)t.GetOutput(typeof(Stream))){
                                innernode.Load(output);
                            }
                            break;
                    }
                    if (s == null){
                        errMsg += "\nCHYBA : transformácia bola neúspešná " + alghoritm;
                        return false;
                    }
                }
                // digest method 
                var digestmethod = nref.SelectSingleNode(".//ds:DigestMethod", ns).Attributes["Algorithm"].Value;
                byte[] hash = null;
                switch (digestmethod){
                    case "http://www.w3.org/2001/04/xmlenc#sha256":
                        SHA256 sha256 = SHA256.Create();
                        hash = sha256.ComputeHash(s);
                        break;
                    default:
                        errMsg += "\nCHYBA : Neznáma hodnota hashovacieho algoritmu : " + digestmethod;
                        return false;
                }
                // read in digest value 
                var digestvalue = nref.SelectSingleNode(".//ds:DigestValue", ns).InnerText;
                // compare digest values 
                string base64hash = Convert.ToBase64String(hash);
                if (!digestvalue.Equals(base64hash)){
                    errMsg += "\nCHYBA : overenie hodnoty ds:DigestValue" +
                        nref.Attributes["URI"].Value + "\nHodnota digestValue = " + digestvalue + "\nHodnota pocitaneho hashu = " + base64hash;
                }
            }
            if (errMsg != ""){
                return false;
            }
            return true;
        }

        private static bool ValidateSignatureValue(XmlDocument doc, XmlNamespaceManager ns, String path, out string errorMessage)
        {
            errorMessage = "";
            string err = "";
            //kanonikalizácia ds:SignedInfo a overenie hodnoty ds:SignatureValue pomocou pripojeného podpisového certifikátu v ds:KeyInfo
            //zoberiem SIGNED INFO prezeniem kanonikalizaciou a zahasujem 
            XmlNode signedInfoN = doc.SelectSingleNode(@"//ds:SignedInfo", ns);
            // najdem sposob kanonikalizacie 
            string signedInfoTransformAlg = doc.SelectSingleNode(@"//ds:SignedInfo/ds:CanonicalizationMethod", ns).Attributes.GetNamedItem("Algorithm").Value;
            //najdem sposob vytvorenia podpisu 
            string signedInfoSignatureAlg = doc.SelectSingleNode(@"//ds:SignedInfo/ds:SignatureMethod", ns).Attributes.GetNamedItem("Algorithm").Value;
            // kanonikalizacia 
            XmlDocument innernode = new XmlDocument(); innernode.PreserveWhitespace = true;
            innernode.LoadXml(signedInfoN.OuterXml);
            Stream s = null;
            switch (signedInfoTransformAlg){
                case "http://www.w3.org/TR/2001/REC-xml-c14n-20010315":
                    // CANONICAL XML VERSION 1.0
                    XmlDsigC14NTransform t = new XmlDsigC14NTransform();
                    t.Algorithm = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
                    if (s == null){
                        t.LoadInput(innernode);
                    }else{
                        t.LoadInput(s);
                    }
                    s = (Stream)t.GetOutput(typeof(Stream));
                    using (Stream output = (Stream)t.GetOutput(typeof(Stream))){
                        innernode.Load(output);
                    }
                    break;
                default:
                    err += Environment.NewLine + "CHYBA: neznáma transformácia " + signedInfoTransformAlg;
                    errorMessage += Environment.NewLine + "CHYBA: kanonikalizácia ds:SignedInfo a overenie hodnoty ds:SignatureValue pomocou pripojeného podpisového certifikátu v ds:KeyInfo:" + err;
                    return false;
            }
            byte[] c14nSignedInfoBytes = ToByteArray(s);
            //zoberiem verejny kluc je pod SignatureValue sekcia x509certificate kde sa nachadza podpisovy certifikat a obsahuje verejny kluc  
            var cert = doc.SelectSingleNode(@"//ds:KeyInfo/ds:X509Data/ds:X509Certificate", ns);
            //var cert = doc.SelectSingleNode(@"//ds:X509Certificate", ns);
            if (cert == null){
                err += Environment.NewLine + "CHYBA: Nenašiel sa element //ds:KeyInfo/ds:X509Data/ds:X509Certificate";
                errorMessage += Environment.NewLine + "CHYBA: kanonikalizácia ds:SignedInfo a overenie hodnoty ds:SignatureValue pomocou pripojeného podpisového certifikátu v ds:KeyInfo:" + err;
                return false;
            }
            byte[] signatureCertificate = Convert.FromBase64String(cert.InnerText);
            // zoberiem hodnotu SIGNATURE VALUE je to Base64 dekodovat na bytey
            XmlNode node = doc.SelectSingleNode(@"//ds:SignatureValue", ns);
            if (node == null){
                err += Environment.NewLine + "CHYBA: Nenašiel sa element //ds:SignatureValue s ns" + ns.LookupNamespace("ds");
                errorMessage += Environment.NewLine + "CHYBA: kanonikalizácia ds:SignedInfo a overenie hodnoty ds:SignatureValue pomocou pripojeného podpisového certifikátu v ds:KeyInfo:" + err;
                return false;
            }
            byte[] signature = Convert.FromBase64String(node.InnerText);
            // z certifikatu vyberiem verejny kluc
            SubjectPublicKeyInfo ski = X509CertificateStructure.GetInstance(Asn1Object.FromByteArray(signatureCertificate)).SubjectPublicKeyInfo;
            AsymmetricKeyParameter pk = PublicKeyFactory.CreateKey(ski);
            string algStr = ""; //signature alg
            //find digest method
            switch (signedInfoSignatureAlg)
            {
                case "http://www.w3.org/2000/09/xmldsig#rsa-sha1":
                    algStr = "sha1";break;
                case "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256":
                    algStr = "sha256";break;
                case "http://www.w3.org/2001/04/xmldsig-more#rsa-sha384":
                    algStr = "sha384";break;
                case "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512":
                    algStr = "sha512";break;
                default:
                    err += Environment.NewLine + "CHYBA: ValidateSignatureValue: Unknown hash algorithm = " + signedInfoSignatureAlg;
                    errorMessage += Environment.NewLine + "CHYBA: kanonikalizácia ds:SignedInfo a overenie hodnoty ds:SignatureValue pomocou pripojeného podpisového certifikátu v ds:KeyInfo:" + err;
                    return false;
            }
            //rozsifrujem RSA - find encryption
            switch (ski.AlgorithmID.ObjectID.Id)
            {
                case "1.2.840.10040.4.1": //dsa
                    algStr += "withdsa";
                    break;
                case "1.2.840.113549.1.1.1": //rsa
                    algStr += "withrsa";
                    break;
                default:
                    err += Environment.NewLine + "CHYBA: ValidateSignatureValue: Unknown key algId = " + ski.AlgorithmID.ObjectID.Id;
                    errorMessage += Environment.NewLine + "CHYBA: kanonikalizácia ds:SignedInfo a overenie hodnoty ds:SignatureValue pomocou pripojeného podpisového certifikátu v ds:KeyInfo:" + err;
                    return false;
            }
            ISigner verif = SignerUtilities.GetSigner(algStr);
            verif.Init(false, pk);
            verif.BlockUpdate(c14nSignedInfoBytes, 0, c14nSignedInfoBytes.Length);
            bool res = verif.VerifySignature(signature);
            if (!res){
                err += Environment.NewLine + "CHYBA: ValidateSignatureValue: VerifySignature=false;";
                errorMessage += Environment.NewLine + "CHYBA : kanonikalizácia ds:SignedInfo a overenie hodnoty ds:SignatureValue pomocou pripojeného podpisového certifikátu v ds:KeyInfo:" + err;
            }
            return res;
        }


        private static bool XmlSignatureFormalValidation(XmlDocument doc, XmlNamespaceManager ns, String path, out string errMsg)
        {
            errMsg = "";
            string status = "";
            // ds:Signature: // •musí mať Id atribút, // •musí mať špecifikovaný namespace xmlns:ds,
            XmlNode dsSignature = doc.SelectSingleNode("//ds:Signature", ns);
            if (dsSignature == null){
                status += Environment.NewLine + "CHYBA: nenasiel sa element ds:Signature s namespace pre prefix ds: " + ns.LookupNamespace("ds");
            }else{
                XmlNode idAtt = dsSignature.Attributes.GetNamedItem("Id");
                if (idAtt == null){
                    status += Environment.NewLine + "CHYBA: ds:Signature nemá Id atribút!";
                }
            }
            // ds: SignatureValue – musí mať Id atribút
            XmlNode dsSignatureValue = doc.SelectSingleNode("//ds:SignatureValue", ns);
            if (dsSignature == null){
                status += Environment.NewLine + "CHYBA: nenasiel sa element ds:SignatureValue s namespace pre prefix ds: " + ns.LookupNamespace("ds");
            }
            else{
                XmlNode idAtt = dsSignatureValue.Attributes.GetNamedItem("Id");
                if (idAtt == null){
                    status += Environment.NewLine + "CHYBA: ds:SignatureValue nemá Id atribút!";
                }
            }
            //	overenie existencie referencií v ds:SignedInfo a hodnôt atribútov Id a Type voči profilu XAdES_ZEP pre:
            //•	ds: KeyInfo element,
            //•	ds: SignatureProperties element,
            //•	xades: SignedProperties element,
            //•	všetky ostatné referencie v rámci ds:SignedInfo musia byť referenciami na ds:Manifest elementy,
            string outerr = "";
            ValidateSignedInfoFormal(doc,dsSignature, ns, out outerr);
            status += outerr;
            //	overenie obsahu ds: SignatureProperties:
            //•	musí mať Id atribút,
            //•	musí obsahovať dva elementy ds: SignatureProperty pre xzep: SignatureVersion a xzep: ProductInfos,
            //•	obidva ds:SignatureProperty musia mať atribút Target nastavený na ds:Signature,
            outerr = "";
            ValidateSignaturePropertiesFormal(doc, ns, out outerr);
            status += outerr;

            outerr = "";
            ValidateManifestElementsFormal(doc, ns, out outerr);
            status += outerr;

            if (status == "")
            {
                errMsg = "\n\n XML SIGNATURE FORMAL VALIDATION: OK";
                return true;
            }
            else
            {
                errMsg = "\n\n XML SIGNATURE FORMAL VALIDATION " + status;
                return false;
            }
        }

        private static bool ValidateSignedInfoFormal(XmlDocument doc,XmlNode dsSignature, XmlNamespaceManager ns, out string status)
        {
            status = "";
            //	overenie existencie referencií v ds:SignedInfo a hodnôt atribútov Id a Type voči profilu XAdES_ZEP pre:
            //•	ds: KeyInfo element,
            //•	ds: SignatureProperties element,
            //•	xades: SignedProperties element,
            //•	všetky ostatné referencie v rámci ds:SignedInfo musia byť referenciami na ds:Manifest elementy,
            XmlNode dsSignedInfo = doc.SelectSingleNode("//ds:SignedInfo", ns);
            var refs = new Dictionary<string, bool>();
            refs["ds:KeyInfo"] = false; refs["ds:SignatureProperties"] = false; refs["xades:SignedProperties"] = false; refs["ds:Manifest"] = false;
            if (dsSignedInfo == null){
                status += Environment.NewLine + "CHYBA: nenasiel sa element ds:SignedInfo s namespace pre prefix ds: " + ns.LookupNamespace("ds");
            }else{
                // prejdem kazdu referenciu a skontrolujem ci existuje
                var nodes = dsSignedInfo.SelectNodes(".//ds:Reference", ns);
                foreach (XmlNode nref in nodes){
                    var type = nref.Attributes["Type"].Value;
                    var uri = nref.Attributes["URI"];
                    var str = uri.Value.TrimStart(new char[] { '#' });
                    XmlNode dereferencedNode = doc.SelectSingleNode(@"//node()[@Id='" + str + "']");
                    if (dereferencedNode == null){
                        status += Environment.NewLine + "CHYBA: nespravna referencia. Referencovany objekt neexistuje: TYPE: " + type + " URI " + str;
                    }
                    // dereference uri
                    switch (type){
                        // hodnota pre ds:KeyInfo
                        case "http://www.w3.org/2000/09/xmldsig#Object":
                            refs["ds:KeyInfo"] = true;
                            if (dereferencedNode != null && dereferencedNode.Name != "ds:KeyInfo"){
                                status += Environment.NewLine + "CHYBA: nespravna referencia. Referencovany objekt  nie je rovnaky ako deklarovany : TYPE: " + type + " URI " + uri + " dereferencedNode.Name " + dereferencedNode.Name;
                            }
                            if (dereferencedNode != null){
                                string err = "";
                                ValidateKeyInfoElement(doc, dereferencedNode, ns, out err);
                                status += err;
                            }
                            break;
                        // hodnota pre ds:SignatureProperties
                        case "http://www.w3.org/2000/09/xmldsig#SignatureProperties":
                            refs["ds:SignatureProperties"] = true;
                            if (dereferencedNode != null && dereferencedNode.Name != "ds:SignatureProperties"){
                                status += Environment.NewLine + "CHYBA: nespravna referencia. Referencovany objekt  nie je rovnaky ako deklarovany : TYPE: " + type + " URI " + uri + " dereferencedNode.Name " + dereferencedNode.Name;
                            }
                            break;
                        // hodnota pre xades:SignedProperties
                        case "http://uri.etsi.org/01903#SignedProperties":
                            refs["xades:SignedProperties"] = true;
                            if (dereferencedNode != null && dereferencedNode.Name != "xades:SignedProperties"){
                                status += Environment.NewLine + "CHYBA: nespravna referencia. Referencovany objekt  nie je rovnaky ako deklarovany : TYPE: " + type + " URI " + uri + " dereferencedNode.Name " + dereferencedNode.Name;
                            }
                            break;
                        // hodnota pre všetky ostatné referencie
                        case "http://www.w3.org/2000/09/xmldsig#Manifest":
                            refs["ds:Manifest"] = true;
                            if (dereferencedNode != null && dereferencedNode.Name != "ds:Manifest"){
                                status += Environment.NewLine + "CHYBA: nespravna referencia. Referencovany objekt  nie je rovnaky ako deklarovany : TYPE: " + type + " URI " + uri + " dereferencedNode.Name " + dereferencedNode.Name;
                            }
                            break;
                        // inak je type nespravny
                        default:
                            status += Environment.NewLine + "CHYBA: nesprávny typ referencie : TYPE: " + type;
                            break;
                    }
                }
                if (refs.ContainsValue(false)){
                    status += Environment.NewLine + "CHYBA: zoznam referencii je nekompletny.";
                }
            }
            if (status != ""){
                return false;
            }else{
                return true;
            }
        }

        private static bool ValidateKeyInfoElement(XmlDocument doc, XmlNode keyInfoNode, XmlNamespaceManager ns, out string errMsg)
        {
            //	overenie obsahu ds: KeyInfo
            //•	musí mať Id atribút,
            errMsg = "";
            var idAtt = keyInfoNode.Attributes.GetNamedItem("Id");
            if (idAtt == null){
                errMsg += Environment.NewLine + "CHYBA: ds:KeyInfo nemá Id atribút!";
            }
            //•	musí obsahovať ds: X509Data, ktorý obsahuje elementy: ds: X509Certificate, ds: X509IssuerSerial, ds: X509SubjectName,
            var x509data = keyInfoNode.SelectSingleNode("./ds:X509Data", ns);
            if (x509data == null){
                errMsg += Environment.NewLine + "CHYBA: ds:KeyInfo nemá x509data element!";
            }else{
                var x509certificate = x509data.SelectSingleNode("./ds:X509Certificate", ns);
                var x509issuerSerial = x509data.SelectSingleNode("./ds:X509IssuerSerial", ns);
                var x509issuerName = x509data.SelectSingleNode("./ds:X509IssuerSerial/ds:X509IssuerName", ns);
                var x509SerialNumber = x509data.SelectSingleNode("./ds:X509IssuerSerial/ds:X509SerialNumber", ns);
                var x509SubjectName = x509data.SelectSingleNode("./ds:X509SubjectName", ns);
                if (x509certificate == null){
                    errMsg += Environment.NewLine + "CHYBA: ds:X509data nemá x509certificate element!";
                }else{
                    // ziskaj hodnoty z certifikatu 
                    X509CertificateStructure x509 = X509CertificateStructure.GetInstance(Asn1Object.FromByteArray(Convert.FromBase64String(x509certificate.InnerText)));
                    if (x509issuerSerial == null){
                        errMsg += Environment.NewLine + "CHYBA: ds:X509data nemá x509IssuerSerial element!";
                    }
                    else{
                        if (x509issuerName == null || x509SerialNumber == null){
                            errMsg += Environment.NewLine + "CHYBA: ds:X509issuerSerial nemá potrebné elementy!";
                        }else{
                            //hodnoty elementov ds: X509IssuerSerial a ds: X509SubjectName súhlasia s príslušnými hodnatami v certifikáte, ktorý sa nachádza v ds: X509Certificate,
                            var issuerNameFromCert = x509.Issuer.ToString();
                            var issuerNameFromXml = x509issuerName.InnerText;
                            var dictFromCert = Util.parseIssuerName(issuerNameFromCert);
                            var dictFromXml = Util.parseIssuerName(issuerNameFromXml);
                            dictFromCert.Equals(dictFromXml);
                            if (dictFromCert["CN"] != dictFromXml["CN"]){
                                errMsg += Environment.NewLine + "CHYBA: ds:X509issuerName " + issuerNameFromXml + " sa nezhoduje s " + issuerNameFromCert;
                            }
                            var serialNumber = x509.SerialNumber.ToString();
                            if (serialNumber != x509SerialNumber.InnerText){
                                errMsg += Environment.NewLine + "CHYBA: ds:X509serialNumber " + x509SerialNumber.InnerText + " sa nezhoduje s " + serialNumber;
                            }
                        }
                    }
                    if (x509SubjectName == null){
                        errMsg += Environment.NewLine + "CHYBA: ds:X509data nemá x509SubjectName element!";
                    }else{
                        var subjectNameFromCert = x509.Subject.ToString();
                        var subjectNameFromXml = x509SubjectName.InnerText;
                        if (subjectNameFromCert != subjectNameFromXml){
                            errMsg += Environment.NewLine + "CHYBA: ds:X509SubjectName " + subjectNameFromXml + " sa nezhoduje s " + subjectNameFromCert;
                        }
                    }
                }

            }
            if (errMsg != ""){
                return false;
            }else{
                return true;
            }

        }

        private static bool ValidateSignaturePropertiesFormal(XmlDocument doc, XmlNamespaceManager ns, out string status)
        {
            status = "";
            //	overenie obsahu ds: SignatureProperties:
            //•	musí mať Id atribút,
            //•	musí obsahovať dva elementy ds: SignatureProperty pre xzep: SignatureVersion a xzep: ProductInfos,
            //•	obidva ds:SignatureProperty musia mať atribút Target nastavený na ds:Signature,
            XmlNode dsSignatureProperties = doc.SelectSingleNode("//ds:SignatureProperties", ns);
            if (dsSignatureProperties == null){
                status += Environment.NewLine + "CHYBA: nenasiel sa element ds:SignatureProperties s namespace pre prefix ds: " + ns.LookupNamespace("ds");
            }else{
                XmlNode idAtt = dsSignatureProperties.Attributes.GetNamedItem("Id");
                if (idAtt == null){
                    status += Environment.NewLine + "CHYBA: ds:SignatureProperties nemá Id atribút!";
                }
            }
            XmlNodeList childs = dsSignatureProperties.SelectNodes("./*", ns);
            // musí obsahovať dva elementy ds: SignatureProperty pre xzep: SignatureVersion a xzep: ProductInfos,
            if (childs.Count != 2){
                status += Environment.NewLine + "CHYBA: ds:SignatureProperties neobsahuje 2 signatureProperty elementy";
            }else{
                int i = 0;
                foreach (XmlNode node in childs){
                    if (node.Name != "ds:SignatureProperty"){
                        status += Environment.NewLine + "CHYBA: ds:SignatureProperties  obsahuje element:" + node.Name;
                        continue;
                    }
                    if (i == 0 && node.SelectSingleNode("./xzep:SignatureVersion", ns) == null){
                        status += Environment.NewLine + "CHYBA: ds:SignatureProperty  neobsahuje element xzep:SignatureVersion";
                    }else if (i == 1 && node.SelectSingleNode("./xzep:ProductInfos", ns) == null){
                        status += Environment.NewLine + "CHYBA: ds:SignatureProperty  neobsahuje element xzep:ProductInfos.";
                    }
                    // obidva ds:SignatureProperty musia mať atribút Target nastavený na ds:Signature,
                    var attTarget = node.Attributes.GetNamedItem("Target");
                    if (attTarget == null){
                        status += Environment.NewLine + "CHYBA: ds:SignatureProperty  neobsahuje atribút Target";
                    }else{
                        var str = attTarget.Value.TrimStart(new char[] { '#' });
                        XmlNode dereferencedNode = doc.SelectSingleNode(@"//node()[@Id='" + str + "']", ns);
                        if (dereferencedNode == null || dereferencedNode.Name != "ds:Signature"){
                            status += Environment.NewLine + "CHYBA: ds:SignatureProperty nemá atribút Target nastavený na ds:Signature: " + str;
                        }
                    }
                    i++;
                }
            }
            if (status != ""){
                return false;
            }
            else{
                return true;
            }
        }

        private static bool ValidateManifestElementsFormal(XmlDocument doc, XmlNamespaceManager ns, out string errMsg)
        {
            errMsg = "";
            //	overenie ds:Manifest elementov:
            //•	každý ds:Manifest element musí mať Id atribút,
            //•	ds: Transforms musí byť z množiny podporovaných algoritmov pre daný element podľa profilu XAdES_ZEP,
            List<string> availableAlghoritms = new List<string>() { "http://www.w3.org/TR/2001/REC-xml-c14n-20010315",
                "http://www.w3.org/2000/09/xmldsig#base64" };

            //•	ds: DigestMethod – musí obsahovať URI niektorého z podporovaných algoritmov podľa profilu XAdES_ZEP,
            List<string> availableDigest = new List<string>() { "http://www.w3.org/2000/09/xmldsig#sha1",
                "http://www.w3.org/2001/04/xmldsigmore#sha224", "http://www.w3.org/2001/04/xmlenc#sha256", "http://www.w3.org/2001/04/xmldsigmore#sha384", "http://www.w3.org/2001/04/xmlenc#sha512" };
            
            //•	overenie hodnoty Type atribútu voči profilu XAdES_ZEP -  "http://www.w3.org/2000/09/xmldsig#Object"
            //•	každý ds:Manifest element musí obsahovať práve jednu referenciu na ds: Object,
            XmlNodeList dsManifestElms = doc.SelectNodes("//ds:Manifest",ns);
            foreach(XmlNode dsManifest in dsManifestElms){
                if(dsManifest.Attributes.GetNamedItem("Id") == null){
                    errMsg += Environment.NewLine + "CHYBA: ds:Manifest nemá atribút Id ";
                }
                //•	ds: Transforms musí byť z množiny podporovaných algoritmov pre daný element podľa profilu XAdES_ZEP,
                XmlNodeList dsTransforms = dsManifest.SelectNodes(".//ds:Transform", ns);
                foreach(XmlNode dsTransform in dsTransforms){
                    if(!availableAlghoritms.Contains(dsTransform.Attributes.GetNamedItem("Algorithm").Value)){
                        errMsg += Environment.NewLine + "CHYBA: ds:Manifest má nepovoleny ds:Transform algoritmus " + dsTransform.Attributes.GetNamedItem("Algorithm").Value;
                    }
                }
                //•	ds: DigestMethod – musí obsahovať URI niektorého z podporovaných algoritmov podľa profilu XAdES_ZEP,
                XmlNodeList dsDigests = dsManifest.SelectNodes(".//ds:DigestMethod", ns);
                foreach (XmlNode dsDigest in dsDigests){
                    if (!availableDigest.Contains(dsDigest.Attributes.GetNamedItem("Algorithm").Value)){
                        errMsg += Environment.NewLine + "CHYBA: ds:Manifest má nepovoleny ds:DigestMethod algoritmus " + dsDigest.Attributes.GetNamedItem("Algorithm").Value;
                    }
                }
                //•	overenie hodnoty Type atribútu voči profilu XAdES_ZEP -  "http://www.w3.org/2000/09/xmldsig#Object"
                XmlNodeList dsReferences = dsManifest.SelectNodes(".//ds:Reference", ns);
                if (dsReferences.Count > 1) {
                    errMsg += Environment.NewLine + "CHYBA: ds:Manifest má viac ako jednu referenciu na Object ";
                }
                foreach (XmlNode dsRef in dsReferences){
                    if (dsRef.Attributes.GetNamedItem("Type").Value != "http://www.w3.org/2000/09/xmldsig#Object"){
                        errMsg += Environment.NewLine + "CHYBA: ds:Manifest má nepovoleny ds:Reference type " + dsRef.Attributes.GetNamedItem("Type").Value;
                    }
                }
            }
            if (errMsg != ""){
                return false;
            }else{
                return true;
            }
        }
        
        private static bool ManifestRefsValidation(XmlDocument doc, XmlNamespaceManager ns, String path, out string errMsg)
        {
            var status = "";
            string err = "";
            // Validate every reference in every ds:manifest
            XmlNodeList manifests = doc.SelectNodes("//ds:Manifest", ns);
            var ret = true;
            foreach (XmlNode manifest in manifests){
                err = "";
                // Validate ds:manifest
                if (!ValidateReferencesInNode(manifest, doc, ns, out err)){
                    ret = false;
                };
                status += err;
            }
            if (status == ""){
                errMsg = "\n\n MANIFEST REFERENCES VALIDATION: OK";
            }else{
                errMsg = "\n\n MANIFEST REFERENCES VALIDATION: " + status;
            }
            return ret;
        }


        private static byte[] ToByteArray(this Stream stream)
        {
            stream.Position = 0;
            byte[] buffer = new byte[stream.Length];
            for (int totalBytesCopied = 0; totalBytesCopied < stream.Length;)
                totalBytesCopied += stream.Read(buffer, totalBytesCopied, Convert.ToInt32(stream.Length) - totalBytesCopied);
            return buffer;
        }


        private static string StreamToString(Stream stream)
        {
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }


    }


}
