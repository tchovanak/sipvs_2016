﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIPVS_2016
{
    class InvoiceItem
    {
        public String name { get; set; }
        public int tax { get; set; }
        public int units { get; set; }
        public double unit_price { get; set; }
    }
}
