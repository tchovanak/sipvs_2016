﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.IO;
using System.Windows.Forms;
using Ditec.Zep.DSigXades.Plugins;
using Ditec.Zep.DSigXades;

namespace SIPVS_2016
{
    public class Subscriber
    {
        // transform xml through xslt to html 
        public static void subscribeXML(string id, string xslPath, string xmlPath, string xsdPath)
        {
            XmlPlugin xmlPlugin = new XmlPlugin();
            string xmlString = System.IO.File.ReadAllText(xmlPath);
            string xsdString = System.IO.File.ReadAllText(xsdPath);
            
            string xslString = System.IO.File.ReadAllText(xslPath);
            var ditecObject = xmlPlugin.CreateObject2("id1","FAKTURA2016", xmlString, xsdString, "http://www.fiit.stuba.sk/SIPVS",
                "http://www.fiit.stuba.sk/SIPVS/schema.xsd", xslString, "http://www.fiit.stuba.sk/SIPVS/transform.xsl", "HTML");
            //var ditecObject2 = xmlPlugin.CreateObject2("id2", "FAKTURA2017", xmlString, xsdString, "http://www.fiit.stuba.sk/SIPVS",
            //    "http://www.fiit.stuba.sk/SIPVS/schema.xsd", xslString, "http://www.fiit.stuba.sk/SIPVS/transform.xsl", "HTML");
            if (!xmlPlugin.ErrorMessage.Equals("")) { MessageBox.Show(xmlPlugin.ErrorMessage); }
            XadesSig signer = new XadesSig();
            
            var mes = signer.AddObject(ditecObject);
            if (!signer.ErrorMessage.Equals("")) { MessageBox.Show(signer.ErrorMessage); }

            //mes = signer.AddObject(ditecObject2);
            //if (!signer.ErrorMessage.Equals("")) { MessageBox.Show(signer.ErrorMessage); }

            signer.Sign("id3", "http://www.w3.org/2001/04/xmlenc#sha256",
                "urn:oid:1.3.158.36061701.1.2.1");
            if (!signer.ErrorMessage.Equals("")) {
                MessageBox.Show(signer.ErrorMessage);
            }else
            {
                string signedXML = signer.SignedXmlWithEnvelope;
                File.WriteAllText(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                            "\\outputs\\signedxml_faktura_" + id + ".xml", signedXML);

            }


        }
    }
}
