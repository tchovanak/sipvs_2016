﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Schema;

namespace SIPVS_2016
{
    class Validator
    {

        public static void validateXML(string schemaPath, string xmlPath, InvoiceForm form)
        {
            // create schemaSet object with some namespace
            XmlSchemaSet schemaSet = new XmlSchemaSet();
            // null value says 'read namespace from the schema'
            schemaSet.Add(null, schemaPath);
            Validate(xmlPath, schemaSet, form);
        }

        private static void Validate(String filename, XmlSchemaSet schemaSet, InvoiceForm form)
        {
            Console.WriteLine();
            Console.WriteLine("\r\nValidating XML file {0}...", filename.ToString());

            XmlSchema compiledSchema = null;

            foreach (XmlSchema schema in schemaSet.Schemas())
            {
                compiledSchema = schema;
            }

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.Schemas.Add(compiledSchema);
            settings.ValidationEventHandler += new ValidationEventHandler(form.ValidationCallback);
            settings.ValidationType = ValidationType.Schema;

            // Confirm the file exists
            if (!File.Exists(filename))
            {
                throw new IOException("File not found");
            }

            // create the schema validating reader
            XmlReader vreader = XmlReader.Create(filename, settings);

            while (vreader.Read()) { }

            // close the reader
            vreader.Close();
        }


    }
}
