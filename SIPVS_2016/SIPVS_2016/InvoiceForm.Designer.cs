﻿namespace SIPVS_2016
{
    partial class InvoiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnTransformHtml = new System.Windows.Forms.Button();
            this.btnValidate = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtSupplierTel = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSupplierIC = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSupplierDIC = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSupplierEmail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSupplierCity = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSupplierZip = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSupplierStreet = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSupplierName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtBuyerTel = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtBuyerIC = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtBuyerDIC = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtBuyerEmail = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtBuyerCity = new System.Windows.Forms.TextBox();
            this.txtBuyerStreet = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBuyerZip = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBuyerName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtInvoiceNumber = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtActualDate = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtIssueDate = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unitsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unitpriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invoiceItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtResponsiblePerson = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.invoiceItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(35, 616);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(154, 54);
            this.btnAdd.TabIndex = 22;
            this.btnAdd.Text = "Pridať";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnTransformHtml
            // 
            this.btnTransformHtml.Location = new System.Drawing.Point(204, 616);
            this.btnTransformHtml.Name = "btnTransformHtml";
            this.btnTransformHtml.Size = new System.Drawing.Size(147, 54);
            this.btnTransformHtml.TabIndex = 23;
            this.btnTransformHtml.Text = "Zobraziť(HTML)";
            this.btnTransformHtml.UseVisualStyleBackColor = true;
            this.btnTransformHtml.Click += new System.EventHandler(this.btnTransform_Click);
            // 
            // btnValidate
            // 
            this.btnValidate.Location = new System.Drawing.Point(376, 616);
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.Size = new System.Drawing.Size(158, 54);
            this.btnValidate.TabIndex = 24;
            this.btnValidate.Text = "Overiť";
            this.btnValidate.UseVisualStyleBackColor = true;
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtSupplierTel);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.txtSupplierIC);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txtSupplierDIC);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtSupplierEmail);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtSupplierCity);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtSupplierZip);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtSupplierStreet);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtSupplierName);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(35, 123);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(426, 244);
            this.panel1.TabIndex = 5;
            // 
            // txtSupplierTel
            // 
            this.txtSupplierTel.Location = new System.Drawing.Point(96, 216);
            this.txtSupplierTel.Name = "txtSupplierTel";
            this.txtSupplierTel.Size = new System.Drawing.Size(315, 20);
            this.txtSupplierTel.TabIndex = 12;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(18, 219);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(22, 13);
            this.label21.TabIndex = 13;
            this.label21.Text = "Tel";
            // 
            // txtSupplierIC
            // 
            this.txtSupplierIC.Location = new System.Drawing.Point(96, 191);
            this.txtSupplierIC.Name = "txtSupplierIC";
            this.txtSupplierIC.Size = new System.Drawing.Size(315, 20);
            this.txtSupplierIC.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 194);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "IC";
            // 
            // txtSupplierDIC
            // 
            this.txtSupplierDIC.Location = new System.Drawing.Point(96, 165);
            this.txtSupplierDIC.Name = "txtSupplierDIC";
            this.txtSupplierDIC.Size = new System.Drawing.Size(315, 20);
            this.txtSupplierDIC.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 168);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "DIC";
            // 
            // txtSupplierEmail
            // 
            this.txtSupplierEmail.Location = new System.Drawing.Point(96, 139);
            this.txtSupplierEmail.Name = "txtSupplierEmail";
            this.txtSupplierEmail.Size = new System.Drawing.Size(315, 20);
            this.txtSupplierEmail.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 142);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Email";
            // 
            // txtSupplierCity
            // 
            this.txtSupplierCity.Location = new System.Drawing.Point(285, 113);
            this.txtSupplierCity.Name = "txtSupplierCity";
            this.txtSupplierCity.Size = new System.Drawing.Size(126, 20);
            this.txtSupplierCity.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(227, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Mesto";
            // 
            // txtSupplierZip
            // 
            this.txtSupplierZip.Location = new System.Drawing.Point(96, 113);
            this.txtSupplierZip.Name = "txtSupplierZip";
            this.txtSupplierZip.Size = new System.Drawing.Size(123, 20);
            this.txtSupplierZip.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "PSČ";
            // 
            // txtSupplierStreet
            // 
            this.txtSupplierStreet.Location = new System.Drawing.Point(96, 82);
            this.txtSupplierStreet.Name = "txtSupplierStreet";
            this.txtSupplierStreet.Size = new System.Drawing.Size(315, 20);
            this.txtSupplierStreet.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ulica a číslo";
            // 
            // txtSupplierName
            // 
            this.txtSupplierName.Location = new System.Drawing.Point(96, 56);
            this.txtSupplierName.Name = "txtSupplierName";
            this.txtSupplierName.Size = new System.Drawing.Size(315, 20);
            this.txtSupplierName.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Meno";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "DODÁVATEĽ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 27);
            this.label2.TabIndex = 0;
            this.label2.Text = "ODBERATEĽ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtBuyerTel);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.txtBuyerIC);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.txtBuyerDIC);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.txtBuyerEmail);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.txtBuyerCity);
            this.panel2.Controls.Add(this.txtBuyerStreet);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtBuyerZip);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.txtBuyerName);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(479, 123);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(413, 244);
            this.panel2.TabIndex = 13;
            // 
            // txtBuyerTel
            // 
            this.txtBuyerTel.Location = new System.Drawing.Point(84, 216);
            this.txtBuyerTel.Name = "txtBuyerTel";
            this.txtBuyerTel.Size = new System.Drawing.Size(315, 20);
            this.txtBuyerTel.TabIndex = 20;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 219);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(22, 13);
            this.label22.TabIndex = 15;
            this.label22.Text = "Tel";
            // 
            // txtBuyerIC
            // 
            this.txtBuyerIC.Location = new System.Drawing.Point(84, 187);
            this.txtBuyerIC.Name = "txtBuyerIC";
            this.txtBuyerIC.Size = new System.Drawing.Size(315, 20);
            this.txtBuyerIC.TabIndex = 19;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 194);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 13);
            this.label16.TabIndex = 13;
            this.label16.Text = "IC";
            // 
            // txtBuyerDIC
            // 
            this.txtBuyerDIC.Location = new System.Drawing.Point(84, 161);
            this.txtBuyerDIC.Name = "txtBuyerDIC";
            this.txtBuyerDIC.Size = new System.Drawing.Size(315, 20);
            this.txtBuyerDIC.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 164);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "DIC";
            // 
            // txtBuyerEmail
            // 
            this.txtBuyerEmail.Location = new System.Drawing.Point(84, 139);
            this.txtBuyerEmail.Name = "txtBuyerEmail";
            this.txtBuyerEmail.Size = new System.Drawing.Size(315, 20);
            this.txtBuyerEmail.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 142);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Email";
            // 
            // txtBuyerCity
            // 
            this.txtBuyerCity.Location = new System.Drawing.Point(273, 113);
            this.txtBuyerCity.Name = "txtBuyerCity";
            this.txtBuyerCity.Size = new System.Drawing.Size(126, 20);
            this.txtBuyerCity.TabIndex = 16;
            // 
            // txtBuyerStreet
            // 
            this.txtBuyerStreet.Location = new System.Drawing.Point(84, 85);
            this.txtBuyerStreet.Name = "txtBuyerStreet";
            this.txtBuyerStreet.Size = new System.Drawing.Size(315, 20);
            this.txtBuyerStreet.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(215, 116);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "Mesto";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 88);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Ulica a číslo";
            // 
            // txtBuyerZip
            // 
            this.txtBuyerZip.Location = new System.Drawing.Point(84, 113);
            this.txtBuyerZip.Name = "txtBuyerZip";
            this.txtBuyerZip.Size = new System.Drawing.Size(123, 20);
            this.txtBuyerZip.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 116);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "PSČ";
            // 
            // txtBuyerName
            // 
            this.txtBuyerName.Location = new System.Drawing.Point(84, 56);
            this.txtBuyerName.Name = "txtBuyerName";
            this.txtBuyerName.Size = new System.Drawing.Size(315, 20);
            this.txtBuyerName.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Meno";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(30, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(244, 26);
            this.label17.TabIndex = 5;
            this.label17.Text = "Faktúra - daňový doklad";
            // 
            // txtInvoiceNumber
            // 
            this.txtInvoiceNumber.Location = new System.Drawing.Point(750, 19);
            this.txtInvoiceNumber.Name = "txtInvoiceNumber";
            this.txtInvoiceNumber.Size = new System.Drawing.Size(142, 20);
            this.txtInvoiceNumber.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(662, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(66, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "Číslo faktúry";
            // 
            // txtActualDate
            // 
            this.txtActualDate.Location = new System.Drawing.Point(750, 45);
            this.txtActualDate.Name = "txtActualDate";
            this.txtActualDate.Size = new System.Drawing.Size(142, 20);
            this.txtActualDate.TabIndex = 2;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(636, 48);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(92, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "Dátum vystavenia";
            // 
            // txtIssueDate
            // 
            this.txtIssueDate.Location = new System.Drawing.Point(750, 71);
            this.txtIssueDate.Name = "txtIssueDate";
            this.txtIssueDate.Size = new System.Drawing.Size(142, 20);
            this.txtIssueDate.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(643, 74);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(85, 13);
            this.label20.TabIndex = 19;
            this.label20.Text = "Dátum splatnosti";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(30, 385);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(212, 26);
            this.label23.TabIndex = 22;
            this.label23.Text = "Faktúrované položky";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.unitsDataGridViewTextBoxColumn,
            this.unitpriceDataGridViewTextBoxColumn,
            this.taxDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.invoiceItemBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(35, 434);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(857, 165);
            this.dataGridView1.TabIndex = 21;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Názov";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // unitsDataGridViewTextBoxColumn
            // 
            this.unitsDataGridViewTextBoxColumn.DataPropertyName = "units";
            this.unitsDataGridViewTextBoxColumn.HeaderText = "počet";
            this.unitsDataGridViewTextBoxColumn.Name = "unitsDataGridViewTextBoxColumn";
            // 
            // unitpriceDataGridViewTextBoxColumn
            // 
            this.unitpriceDataGridViewTextBoxColumn.DataPropertyName = "unit_price";
            this.unitpriceDataGridViewTextBoxColumn.HeaderText = "jednotková cena (EUR)";
            this.unitpriceDataGridViewTextBoxColumn.Name = "unitpriceDataGridViewTextBoxColumn";
            // 
            // taxDataGridViewTextBoxColumn
            // 
            this.taxDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.taxDataGridViewTextBoxColumn.DataPropertyName = "tax";
            this.taxDataGridViewTextBoxColumn.HeaderText = "daň(%)";
            this.taxDataGridViewTextBoxColumn.Name = "taxDataGridViewTextBoxColumn";
            this.taxDataGridViewTextBoxColumn.Width = 64;
            // 
            // invoiceItemBindingSource
            // 
            this.invoiceItemBindingSource.DataSource = typeof(SIPVS_2016.InvoiceItem);
            // 
            // txtResponsiblePerson
            // 
            this.txtResponsiblePerson.Location = new System.Drawing.Point(750, 97);
            this.txtResponsiblePerson.Name = "txtResponsiblePerson";
            this.txtResponsiblePerson.Size = new System.Drawing.Size(142, 20);
            this.txtResponsiblePerson.TabIndex = 4;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(628, 100);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(100, 13);
            this.label28.TabIndex = 30;
            this.label28.Text = "Zodpovedná osoba";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(554, 616);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(158, 54);
            this.button1.TabIndex = 31;
            this.button1.Text = "Podpísať";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnSubscribe_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(734, 616);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(158, 54);
            this.button2.TabIndex = 32;
            this.button2.Text = "Rozšír na T formu";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnExtendToTForm_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(35, 676);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(154, 54);
            this.button3.TabIndex = 33;
            this.button3.Text = "Validácia podpisov";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.btnValidateDocuments_Click);
            // 
            // InvoiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 733);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtResponsiblePerson);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.txtIssueDate);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtActualDate);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtInvoiceNumber);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnValidate);
            this.Controls.Add(this.btnTransformHtml);
            this.Controls.Add(this.btnAdd);
            this.Name = "InvoiceForm";
            this.Text = "Faktúra";
            this.Load += new System.EventHandler(this.InvoiceForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.invoiceItemBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnTransformHtml;
        private System.Windows.Forms.Button btnValidate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtSupplierName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSupplierStreet;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSupplierZip;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSupplierCity;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSupplierEmail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSupplierDIC;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSupplierIC;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBuyerIC;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtBuyerDIC;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtBuyerEmail;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtBuyerCity;
        private System.Windows.Forms.TextBox txtBuyerStreet;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBuyerZip;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBuyerName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtInvoiceNumber;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtActualDate;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtIssueDate;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtSupplierTel;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtBuyerTel;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtResponsiblePerson;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.BindingSource invoiceItemBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn taxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitpriceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}