﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Schema;


namespace SIPVS_2016
{
    public partial class InvoiceForm : Form
    {


        private string id = "F1";
        private StringBuilder validationMessage = new StringBuilder();

        public InvoiceForm()
        {
            InitializeComponent();

        }

        private void InvoiceForm_Load(object sender, EventArgs e)
        {

        }

        private XmlNode createContactNode(String name, XmlDocument xmlDoc, String contactname,
            String street, String city, String zip, String email, String tel, String DIC, String IC)
        {

            XmlNode contactNode = xmlDoc.CreateElement(name);
            XmlAttribute nameNode = xmlDoc.CreateAttribute("name");
            XmlNode streetNode = xmlDoc.CreateElement("street");
            XmlNode cityNode = xmlDoc.CreateElement("city");
            XmlNode zipNode = xmlDoc.CreateElement("zip");
            XmlNode emailNode = xmlDoc.CreateElement("email");
            XmlNode telNode = xmlDoc.CreateElement("tel");
            XmlNode DICNode = xmlDoc.CreateElement("DIC");
            XmlNode ICNode = xmlDoc.CreateElement("IC");

            nameNode.Value = contactname;
            streetNode.InnerText = street;
            cityNode.InnerText = city;
            zipNode.InnerText = zip;
            emailNode.InnerText = email;
            telNode.InnerText = tel;
            DICNode.InnerText = DIC;
            ICNode.InnerText = IC;

            contactNode.Attributes.Append(nameNode);
            contactNode.AppendChild(streetNode);
            contactNode.AppendChild(cityNode);
            contactNode.AppendChild(zipNode);
            contactNode.AppendChild(emailNode);
            contactNode.AppendChild(telNode);
            contactNode.AppendChild(DICNode);
            contactNode.AppendChild(ICNode);

            return contactNode;
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {


            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration declaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(declaration);
            XmlAttribute idAtt = xmlDoc.CreateAttribute("id");
            idAtt.Value = this.txtInvoiceNumber.Text;
            XmlNode rootNode = xmlDoc.CreateElement("invoice");
            XmlAttribute xmlns = xmlDoc.CreateAttribute("xmlns");
            xmlns.Value = "http://www.fiit.stuba.sk/SIPVS";
            XmlAttribute xmlnsct = xmlDoc.CreateAttribute("xmlns:ct");
            xmlnsct.Value = "http://www.fiit.stuba.sk/SIPVS/CT";
            rootNode.Attributes.Append(xmlns);
            rootNode.Attributes.Append(xmlnsct);
            XmlNode supplierContactNode = createContactNode("provider", xmlDoc, this.txtSupplierName.Text,
                this.txtSupplierStreet.Text, this.txtSupplierCity.Text, this.txtSupplierZip.Text,
                this.txtSupplierEmail.Text, this.txtSupplierTel.Text, this.txtSupplierDIC.Text,
                this.txtSupplierIC.Text);
            XmlNode buyerContactNode = createContactNode("consumer", xmlDoc, this.txtBuyerName.Text,
                this.txtBuyerStreet.Text, this.txtBuyerCity.Text, this.txtBuyerZip.Text,
                this.txtBuyerEmail.Text, this.txtBuyerTel.Text, this.txtBuyerDIC.Text,
                this.txtBuyerIC.Text);
            XmlNode responsiblePersonNode = xmlDoc.CreateElement("responsible_person");
            responsiblePersonNode.InnerText = this.txtResponsiblePerson.Text;
            XmlNode actualDateNode = xmlDoc.CreateElement("actual_date");
            actualDateNode.InnerText = this.txtActualDate.Text;

            XmlNode issueDateNode = xmlDoc.CreateElement("issue_date");
            issueDateNode.InnerText = this.txtIssueDate.Text;
            XmlNode itemsNode = xmlDoc.CreateElement("items");

            foreach (InvoiceItem it in this.invoiceItemBindingSource.List)
            {
                XmlNode item = xmlDoc.CreateElement("item");
                item.InnerText = it.name;
                XmlAttribute tax = xmlDoc.CreateAttribute("tax");
                tax.Value = it.tax.ToString();
                XmlAttribute units = xmlDoc.CreateAttribute("units");
                units.Value = it.units.ToString();
                XmlAttribute unitPrice = xmlDoc.CreateAttribute("unit_price");
                unitPrice.Value = it.unit_price.ToString();
                XmlAttribute unit_price_with_tax = xmlDoc.CreateAttribute("unit_price_with_tax");
                var taxD = double.Parse(tax.Value);
                var unitsI = int.Parse(units.Value);
                var unitPriceD = double.Parse(unitPrice.Value);
                var unitPriceWithTax = Math.Round((unitsI * unitPriceD) * (1 + taxD / 100), 2);
                unit_price_with_tax.Value = unitPriceWithTax.ToString();
                unitPrice.Value = it.unit_price.ToString();
                item.Attributes.Append(units);
                item.Attributes.Append(unitPrice);
                item.Attributes.Append(tax);
                item.Attributes.Append(unit_price_with_tax);
                itemsNode.AppendChild(item);
            }

            rootNode.Attributes.Append(idAtt);
            rootNode.AppendChild(supplierContactNode);
            rootNode.AppendChild(buyerContactNode);
            rootNode.AppendChild(itemsNode);
            rootNode.AppendChild(actualDateNode);
            rootNode.AppendChild(issueDateNode);
            rootNode.AppendChild(responsiblePersonNode);
            xmlDoc.AppendChild(rootNode);

            try
            {
                xmlDoc.Save(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                           "\\outputs\\faktura_" + this.txtInvoiceNumber.Text + ".xml");
            }
            catch
            {
                MessageBox.Show("Faktúra sa nedá uložiť");
            }

            MessageBox.Show("Faktúra bola úspešne uložená do " + Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                           "\\outputs\\faktura_" + this.txtInvoiceNumber.Text + ".xml");


        }

        private void btnTransform_Click(object sender, EventArgs e)
        {
            Transformator.transformXMLtoHTML(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                            "\\schemes\\XSLTFile.xslt",
                                            Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                            "\\outputs\\faktura_" + this.txtInvoiceNumber.Text + ".xml",
                                            Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                            "\\outputs\\faktura_" + this.txtInvoiceNumber.Text + ".html");
            if (File.Exists(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\outputs\\faktura_" + this.txtInvoiceNumber.Text + ".html"))
            {
                System.Diagnostics.Process.Start(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                              "\\outputs\\faktura_" + this.txtInvoiceNumber.Text + ".html");
            }
            else
            {
                MessageBox.Show("Zatial nebola vytvorena faktura, ktoru by bolo mozne transformovat.");
            }

        }

        private void btnValidate_Click(object sender, EventArgs e)
        {
            this.validationMessage = new StringBuilder();
            try
            {
                Validator.validateXML(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                           "\\schemes\\XMLSchema.xsd",
                                           Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                            "\\outputs\\faktura_" + this.txtInvoiceNumber.Text + ".xml", this);
            }
            catch (IOException)
            {
                MessageBox.Show("Faktúra ešte nebola vytvorená, preto nie je možné ju validovať.");
                return;
            }

            if (this.validationMessage.ToString() == "")
            {
                MessageBox.Show("Dokument je validný podľa schémy.");
            }
            else
            {
                MessageBox.Show(this.validationMessage.ToString());
            }

        }


        private void btnSubscribe_Click(object sender, EventArgs e)
        {
            try
            {
                Subscriber.subscribeXML(this.txtInvoiceNumber.Text, Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                            "\\schemes\\XSLTFile.xslt",
                                           Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                            "\\outputs\\faktura_" + this.txtInvoiceNumber.Text + ".xml",
                                           Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                           "\\schemes\\XMLSchema.xsd"
                                           );
            }
            catch (IOException)
            {
                MessageBox.Show("Faktúra ešte nebola vytvorená, preto nie je možné ju podpísať.");
                return;
            }

        }

        private void btnExtendToTForm_Click(object sender, EventArgs e)
        {
            TimestampExtender.extendToTForm(this.txtInvoiceNumber.Text, 
                                           Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                           "\\outputs\\signedxml_faktura_" + this.txtInvoiceNumber.Text + ".xml"
                                           ,
                                           Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                                           "\\outputs\\");

        }


        // display errors and warnings
        internal void ValidationCallback(object Sender, ValidationEventArgs args)
        {

            this.validationMessage.AppendLine("\tFaktúra obsahuje chyby na riadku : " + args.Exception.LineNumber.ToString());
            if (args.Severity == XmlSeverityType.Warning)
            {
                this.validationMessage.AppendLine("\tWarning: Schéma sa nenašla.  Bez validácie." + args.Message);
            }
            else
            {
                this.validationMessage.AppendLine("\tChyba validácie: " + args.Message);
            }
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            // If the data source raises an exception when a cell value is parsed, display an error message.
            if ((e.Context & DataGridViewDataErrorContexts.Parsing) != 0)
            {
                MessageBox.Show("Vstupná hodnota bola zadaná v nesprávnom tvare...");
            }
        }

        private void btnValidateDocuments_Click(object sender, EventArgs e)
        {
            List<string> docs = new List<string> {
                   "XadesT.xml",
                   "01XadesT.xml",
                   "02XadesT.xml",
                   "03XadesT.xml",
                   "04XadesT.xml",
                   "05XadesT.xml",
                   "06XadesT.xml",
                   "07XadesT.xml",
                   "08XadesT.xml",
                   "09XadesT.xml",
                   "10XadesT.xml",
                   "11XadesT.xml",
                   "12XadesT.xml"
            };
            
            foreach(string doc in docs)
            {
                string path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\signatures\\" + doc;
                SignatureValidator.Validate(path);
            }
            
        }
    }
}
